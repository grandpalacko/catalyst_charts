<?php
$plugin = array(
  'single' => TRUE,
  'title' => t('Chartist.js Chart'),
  'description' => t('Displays a Chartist.js based chart.'),
  'category' => t('Miscellaneous'),
  'edit form' => 'chartist_chart_edit_form',
  'render callback' => 'chartist_chart_render',
  'admin info' => 'chartist_chart_admin_info',
  'admin title' => 'chartist_chart_admin_title',
  'defaults' => array(
    'chart' => NULL,
    'labels' => '',
    'foot' => '',
  ),
  'all contexts' => TRUE,
);

/**
 * Callback for admin info.
 */
function chartist_chart_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = '';
    $block->content = '';
    return $block;
  }
}

/**
 * Callback to provide the administrative title of chart.
 */
function chartist_chart_admin_title($subtype, $conf) {
  $output = t('Chartist.js chart: <em>@chart</em>', array(
    '@chart' => $conf['chart'],
  ));

  return $output;
}

/**
 * 'Edit form' callback for the content type.
 */
function chartist_chart_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $charts = catalyst_charts_info();
  $options = array();
  foreach($charts as $key => $chart) {
    $options[$key] = check_plain($chart['title']);
  }

  $form['chart'] = array(
    '#title' => t('Chart'),
    '#description' => t('List of chart defined by modules.'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['chart'],
    '#required' => TRUE,
  );

  $form['labels'] = array(
    '#title' => t('Labels'),
    '#description' => t('Labels of the chart. Enter one value per line, in the format colorcode|label.'),
    '#type' => 'textarea',
    '#default_value' => $conf['labels'],
    '#required' => FALSE,
  );

  $form['foot'] = array(
    '#title' => t('Foot'),
    '#description' => t('Footnote for the chart.'),
    '#type' => 'textarea',
    '#default_value' => $conf['foot'],
    '#required' => FALSE,
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function chartist_chart_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function chartist_chart_render($subtype, $conf, $args, $contexts) {
  // Get content
  $block = new stdClass();

  // Update the strings to allow contexts.
  if (!empty($contexts)) {
    $conf['foot'] = ctools_context_keyword_substitute($conf['foot'], array(), $contexts);
  }

  $charts = catalyst_charts_info();
  $chart = $charts[$conf['chart']];

  $data = call_user_func($chart['callback']);

  $block->title = check_plain($chart['title']);
  $block->content = array(
    'chart' => catalyst_charts_build_chartist($chart['type'], $data['data'], $data['options'], $data['aspect_ratio']),
  );

  if (!empty($conf['labels'])) {
    $items = array();
    $labels = explode(PHP_EOL, $conf['labels']);

    foreach($labels as $label) {
      $parts = explode('|', $label);

      $items[] = array(
        '<span class="graph__label"><i class="graph__icon" style="background-color: ' .
          check_plain($parts[0]) . '"></i> ' . check_plain($parts[1]) . '</span>',
      );
    }

    if (!empty($items)) {
      $block->content['labels'] = array(
        '#weight' => -10,
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array(
          'class' => 'graph__labels',
        ),
      );
    }
  }

  if (!empty($conf['foot'])) {
    $block->content['foot'] = array(
      '#weight' => 10,
      '#markup' => filter_xss($conf['foot']),
      '#prefix' => '<footer class="graph__foot">',
      '#suffix' => '</footer>',
    );
  }

  return $block;
}