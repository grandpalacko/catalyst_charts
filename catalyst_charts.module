<?php
/**
 * @file
 * catalyst_charts.module file.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function catalyst_charts_ctools_plugin_directory($owner, $plugin_type) {
 if ($owner == 'ctools' && $plugin_type == 'content_types') {
   return 'plugins/' . $plugin_type;
 }
}

/**
 * Implements hook_libraries_info().
 */
function catalyst_charts_libraries_info() {
  $libraries['chartist'] = array(
    'name' => 'Chartist.js',
    'vendor url' => 'http://gionkunz.github.io/chartist-js/index.html',
    'download url' => 'https://github.com/gionkunz/chartist-js/tree/develop/dist',
    'path' => 'dist',
    'version arguments' => array(
      'file' => 'CHANGELOG.md',
      'pattern' => '/v((\d+)\.(\d+)\.(\d+))/',
    ),
    'files' => array(
      'js' => array('chartist.js'),
      'css' => array('chartist.min.css'),
    ),
    'variants' => array(
      'minified' => array(
        'js' => array('chartist.min.js'),
      ),
    ),
    'integration files' => array(
      'catalyst_charts' => array(
        'js' => array(
          'js/chartist.js' => array(),
        ),
      ),
    ),
  );

  $libraries['chartist-plugin-tooltip'] = array(
    'name' => 'Chartist.js Plugin Tooltip',
    'vendor url' => 'https://github.com/Globegitter/chartist-plugin-tooltip',
    'download url' => 'https://github.com/Globegitter/chartist-plugin-tooltip/tree/master/dist',
    'path' => 'dist',
    'version arguments' => array(
      'file' => 'CHANGELOG.md',
      'pattern' => '/((\d+)\.(\d+)\.(\d+))/',
    ),
    'dependencies' => array(
      'chartist',
    ),
    'files' => array(
      'js' => array('chartist-plugin-tooltip.js'),
      'css' => array('chartist-plugin-tooltip.css'),
    ),
    'variants' => array(
      'minified' => array(
        'js' => array('chartist-plugin-tooltip.min.js'),
      ),
    ),
  );

  return $libraries;
}

function catalyst_charts_info() {
  $data = &drupal_static(__FUNCTION__);
  if (!isset($data)) {
    $data = module_invoke_all('charts_info');
    drupal_alter('charts_info', $data);
  }
  return $data;
}

/**
 * List of available aspect ratios.
 *
 * @param string $aspect_ratio
 *   Selected aspect ratio.
 *
 * @return string
 *   The CSS class name of the selected aspect ratio.
 */
function catalyst_charts_chartist_aspect_ratio($aspect_ratio = '3:4') {
  $aspect_ratios = array(
     '1' => 'ct-square',
     '15:16' => 'ct-minor-second',
     '8:9' => 'ct-major-second',
     '5:6' => 'ct-minor-third',
     '4:5' => 'ct-major-third',
     '3:4' => 'ct-perfect-fourth',
     '2:3' => 'ct-perfect-fifth',
     '5:8' => 'ct-minor-sixth',
     '1:1.618' => 'ct-golden-section',
     '3:5' => 'ct-major-sixth',
     '9:16' => 'ct-minor-seventh',
     '8:15' => 'ct-major-seventh',
     '1:2' => 'ct-octave',
     '2:5' => 'ct-major-tenth',
     '3:8' => 'ct-major-eleventh',
     '1:3' => 'ct-major-twelfth',
     '1:4' => 'ct-double-octave',
  );

  // Check if the selected aspect ratio exists
  if (array_key_exists($aspect_ratio, $aspect_ratios)) {
    return $aspect_ratios[$aspect_ratio];
  }

  // Return a default class name
  return 'ct-perfect-fourth';
}

/**
 * Attach Chartist.js settings, library and return renderable array for a chart.
 *
 * @param string $type
 *   Type of chart, e.g.: 'line', 'bar'
 * @param array $data
 *   Associative array of showable data.
 * @param array $options
 *   Associative array of chart options.
 * @param string $aspect_ratio
 *   Chart's aspect ratio.
 *
 * @return array
 *   A renderable array.
 */
function catalyst_charts_build_chartist($type = 'line', $data, $options = array(), $aspect_ratio = '3:4') {
  $chart_id = drupal_html_id('chartist');
  $build = array(
    '#theme' => 'html_tag',
    '#tag' => 'div',
    '#value' => '',
    '#attributes' => array(
      'id' => $chart_id,
      'class' => array(
        'ct-chart',
        catalyst_charts_chartist_aspect_ratio($aspect_ratio),
      ),
    ),
  );

  $settings = array(
    'id' => $chart_id,
    'type' => $type,
    'data' => $data,
    'options' => $options,
  );

  $build['#attached']['js'][] = array(
    'data' => array('chartist' => array($settings)),
    'type' => 'setting',
  );

  // Load the 'chartist' library
  $build['#attached']['libraries_load'][] = array('chartist');

  if (isset($options['enabled_plugins'])) {
    foreach($options['enabled_plugins'] as $plugin) {
      $build['#attached']['libraries_load'][] = array('chartist-plugin-' . $plugin);
    }
  }

  return $build;
}