/**
 * @file
 * Chartist.js integration.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.Chartist = {
    attach: function(context, settings) {
      $(settings.chartist).each(function () {
        var that = this;
        switch (that.type) {
          case 'line':
            var type = 'Line';
            break;
          case 'bar':
            var type = 'Bar';
            break;
          case 'pie':
            var type = 'Pie';
            break;
          default:
            var type = 'Bar';
            break
        }

        $(that.options.enabled_plugins).each(function(index, value) {
          if (value == 'tooltip') {
            that.options.plugins = [Chartist.plugins.tooltip()];
          }
        });

        new Chartist[type]('#' + that.id, that.data, that.options);
      });
    }
  }

}(jQuery));
